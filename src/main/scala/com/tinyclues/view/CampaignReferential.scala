package com.tinyclues.view

import com.redis.RedisClient
import com.tinyclues.ApiConfiguration
import org.slf4j.{Logger, LoggerFactory}

class CampaignReferential(val conf : ApiConfiguration ) {
  val logger : Logger = LoggerFactory.getLogger(classOf[CampaignReferential])

  lazy val client = new RedisClient(conf.redisHost, conf.redisPort)

  def addCampaignTarget(campaignId : String, targetNumber : Int ) : Boolean = {
     val res = client.set(Campaign.keyReferential(campaignId), targetNumber)
      logger.debug(s"[campaign, targetNumber, result ] : ", Array(campaignId, targetNumber, res))
      res
  }

}
