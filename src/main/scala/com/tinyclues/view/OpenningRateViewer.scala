package com.tinyclues.view


import com.redis.{RedisClient, RedisClientPool}
import org.slf4j.{Logger, LoggerFactory}
import com.redis.serialization.Parse.Implicits.parseDouble
import com.tinyclues.ApiConfiguration


class OpenningRateViewer (val conf : ApiConfiguration ) {
  val logger : Logger = LoggerFactory.getLogger(classOf[OpennedEmailViewer])

  lazy val client = new RedisClient(conf.redisHost, conf.redisPort)

  private[view] def getOpenningRate (campaignId : String) : (Option[Double], String)= {

      val target : Option[Double] =  client.get[Double](Campaign.keyReferential(campaignId))
      val opennedEmails = client.pfcount(Campaign.keyOpenned(campaignId))

       calculateRate(campaignId, target, opennedEmails)

  }

  /**
    *
    * @param campaignId will be used in logs
    * @param target targetted people number
    * @param opennedEmails openned email number
    * @return openning rate
    */
  private def calculateRate(campaignId: String, target: Option[Double], opennedEmails : Option[Long]) : (Option[Double], String) = {
    (target, opennedEmails) match {
      case (Some(nbTarget), Some(nbOpennedEmail)) if nbTarget != 0.0 => (Some(nbOpennedEmail.toDouble / nbTarget) , "SUCCESS")

      case (Some(nbTarget), None) =>
        val message = s"error on getting the number of openned emails for ${campaignId}"
        logger.warn(message)
        (Some(0.0), message)

      case (Some(0.0), _) =>
        val message = s"target number for campaign ${campaignId} is zero"
        logger.warn(message)
        (None, message)

      case (None, Some(nbOpennedEmail)) =>
        val message = s"error on getting the number of targetted clients for campaign ${campaignId} "
        logger.warn(message)
        (None, message)

      case (None, None) =>
        val message = s"error when trying to retrieve data for campaign ${campaignId} "
        logger.warn(message)
        (None, message)
    }
  }


  def openningRate(campaignId : String) : OpenningRate= {
    val result = getOpenningRate(campaignId)
    OpenningRate(campaignId, result._1, result._2)
  }
}

case class OpenningRate(campaignId: String,  openningRate: Option[Double], message : String)

