package com.tinyclues.view


import com.tinyclues.ApiConfiguration
import com.redis.RedisClient
import org.slf4j.{Logger, LoggerFactory}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

class OpennedEmailViewer (val conf : ApiConfiguration) {

  val logger : Logger = LoggerFactory.getLogger(classOf[OpennedEmailViewer])

  lazy val redisClient = new RedisClient(conf.redisHost, conf.redisPort)

  def addClient(campaignId : String, clientId : String): Future[Option[Long]] = {
    Future{
      val res = redisClient.pfadd(Campaign.keyOpenned(campaignId), clientId)
      logger.debug(s"[campaign, client, result] : {} ",Array(campaignId, clientId, res))
      res
    }
  }
}
