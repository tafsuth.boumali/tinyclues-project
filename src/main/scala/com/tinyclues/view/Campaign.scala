package com.tinyclues.view

object Campaign {

  private val CampaignPrefix = "campaign"

  private val ReferentialFlag="ref"

  private val OpennedFlag = "openned"

  def keyReferential( id : String) : String = {
    s"${ReferentialFlag}-${CampaignPrefix}-$id"
  }

  def keyOpenned( id : String) : String = {
    s"${OpennedFlag}-${CampaignPrefix}-$id"
  }

}
