package com.tinyclues

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.LoggerFactory
import spray.can.Http
import akka.io.Tcp.{Bound, CommandFailed}
import spray.routing.HttpServiceActor
import akka.pattern.ask
import com.tinyclues.routes.Routes
import com.tinyclues.view.{CampaignReferential, OpennedEmailViewer, OpenningRateViewer}


object ServerBoot extends App with Server {
  override implicit val system: ActorSystem = ActorSystem("campaign")
   start(ConfigFactory.load())
}

trait Server {
  private val logger = LoggerFactory.getLogger(classOf[Server])

  implicit val system: ActorSystem

  def start(configuration: Config): Unit = {

    val actor = system.actorOf(Props(classOf[CampaignApi], configuration), "Api")

    val serverSettings = new ServerConfiguration(configuration)
    val networkInterface = serverSettings.listeningInterface
    val port = serverSettings.listeningPort

    val bounded = IO(Http)(system).ask(Http.Bind(actor, interface = networkInterface, port = port))(serverSettings.bindingTimeout)

    implicit val ctx = system.dispatcher

    bounded onSuccess {
      case _: Bound ⇒ {
        logger.info("server successfully bound to {}:{}", networkInterface, port)
      }
      case _ : CommandFailed⇒ {
        logger.error("server failed to bound to {}:{}", networkInterface, port)
        system.shutdown()
      }
    }

    bounded onFailure {
      case t: RuntimeException ⇒ {
        logger.error(s"binding attempt failed ${t.getMessage}", t)
        system.shutdown()
      }
      case e: Throwable ⇒ {
        logger.error("binding attempt failed with unknown reason!", e)
        system.shutdown()
      }
    }
  }

}


class CampaignApi(baseConfig : Config) extends HttpServiceActor with Routes {

  val configuration = new ApiConfiguration(baseConfig)

  // test with a redisclientPool, integration tests were instables
  // getting Protocol error: Got (v,[B@75106b5) as initial reply byte
  // temporary workaround : new client for each read/write

  lazy override val opennedEmailViewer: OpennedEmailViewer = new OpennedEmailViewer(configuration)
  lazy override val campaignReferential: CampaignReferential = new CampaignReferential(configuration)
  lazy override val openningRateViewer: OpenningRateViewer = new OpenningRateViewer(configuration)

  def receive = runRoute(routes)

}




