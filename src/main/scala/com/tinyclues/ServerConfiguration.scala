package com.tinyclues

import com.typesafe.config.Config

class ServerConfiguration(config: Config) {

    private val confPrefix = "server"
    require(config.hasPath(confPrefix), s"configuration should define ${confPrefix} namespace")
    private val settings = config.getConfig(confPrefix)

    val listeningInterface = settings.getString("listeningInterface")
    val listeningPort = settings.getInt("listeningPort")
    import net.ceedubs.ficus.Ficus._

    import scala.concurrent.duration.FiniteDuration
    val bindingTimeout = settings.as[FiniteDuration]("bindingTimeout")

}
