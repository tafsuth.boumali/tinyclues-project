package com.tinyclues

import com.typesafe.config.Config

class ApiConfiguration(config : Config ) {
  private val confPrefix = "redis"
  require(config.hasPath(confPrefix), s"configuration should define ${confPrefix} namespace")
  private val settings = config.getConfig(confPrefix)

  val redisHost =settings.getString("host")
  val redisPort =  settings.getInt("port")
}

