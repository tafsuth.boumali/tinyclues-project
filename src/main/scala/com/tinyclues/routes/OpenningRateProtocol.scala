package com.tinyclues.routes

import com.tinyclues.view.OpenningRate
import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol


object OpenningRateProtocol extends DefaultJsonProtocol with SprayJsonSupport{
  implicit val OpenningRateFormat = jsonFormat3(OpenningRate)
}
