package com.tinyclues.routes

import com.tinyclues.view.{OpennedEmailViewer, OpenningRate, OpenningRateViewer}
import spray.http.StatusCodes
import spray.routing.{HttpService, Route}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

trait OpenningRateRoute extends HttpService {

  val openningRateViewer: OpenningRateViewer

  import com.tinyclues.routes.OpenningRateProtocol._

  def openningRateRoute: Route = {

    pathPrefix("campaign" / Segment / "openningRate") { campaignId ⇒
      get {
        detach() {
          complete {
            openningRateViewer.openningRate(campaignId)
          }
        }
      }
    }
  }

}