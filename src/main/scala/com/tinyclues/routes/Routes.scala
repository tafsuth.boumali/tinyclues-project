package com.tinyclues.routes

import java.net.SocketException

import com.redis.RedisConnectionException
import com.tinyclues.view.{CampaignReferential, OpennedEmailViewer, OpenningRateViewer}
import spray.http.StatusCodes
import spray.routing.ExceptionHandler

trait Routes extends OpennedEmailRoute with CampaignReferentialRoute with OpenningRateRoute {

  val exceptionRequestHandler = ExceptionHandler {
    case exception : RedisConnectionException ⇒ complete((StatusCodes.InternalServerError, exception.getMessage))
    case exception : SocketException ⇒ complete((StatusCodes.InternalServerError, exception.getMessage))
    case exception @ (_: RuntimeException) ⇒ complete((StatusCodes.InternalServerError, exception.getMessage))
  }

  def routes = handleExceptions(exceptionRequestHandler) {
    pathPrefix ("tinyclues" / "api" / Versions.`V1.0` / "campaigns" ) {
      path("swagger.yaml") {
        getFromResource(s"campaign-v1.0.yaml")
      } ~ referentialRoute ~  opennedEmailRoute ~ openningRateRoute
    }
  }

}

object Versions {
  val `V1.0` = "v1.0"
}

