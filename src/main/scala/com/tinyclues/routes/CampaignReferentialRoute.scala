package com.tinyclues.routes

import com.tinyclues.view.CampaignReferential
import spray.http.StatusCodes
import spray.routing.{HttpService, Route}


trait CampaignReferentialRoute extends HttpService {

  val campaignReferential: CampaignReferential

  def referentialRoute: Route = {
    pathPrefix("campaign" / Segment / "target" / IntNumber) { (campaignId, targetNumber) ⇒
      detach() {
        put {
          campaignReferential.addCampaignTarget(campaignId, targetNumber) match {
            case true => complete(StatusCodes.Created)
            case _ => complete(StatusCodes.BadRequest)
          }
        }
      }
    }
  }
}