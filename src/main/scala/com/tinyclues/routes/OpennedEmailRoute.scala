package com.tinyclues.routes


import com.tinyclues.view.OpennedEmailViewer
import spray.http.StatusCodes
import spray.routing.{HttpService, Route}

trait OpennedEmailRoute extends HttpService{

 val opennedEmailViewer : OpennedEmailViewer
  def opennedEmailRoute: Route = {
      pathPrefix("campaign" / Segment / "client" / Segment) { (campaignId, clientId) ⇒
        get {
            opennedEmailViewer.addClient(campaignId, clientId)
            complete(StatusCodes.Accepted)
        }
      }
    }
}