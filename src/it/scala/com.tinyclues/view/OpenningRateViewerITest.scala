package com.tinyclues.view

import com.tinyclues.ApiConfiguration
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

class OpenningRateViewerITest extends FlatSpec with BeforeAndAfterAll with Matchers {

  val configuration= new ApiConfiguration(ConfigFactory.load)

  "openningRate" should "calculate opening rate for a given campaign" in {
    val campaign =  "fnacMacBook"
    val client1 = "Tafsuth"
    val client2 = "Boumali"

    //fix target number for the campaign
   new CampaignReferential(configuration).addCampaignTarget(campaign, 10)

    //add clients that openned their emails
    val opennedEmailViewer = new OpennedEmailViewer(configuration)
    opennedEmailViewer.addClient(campaign, client1)
    opennedEmailViewer.addClient(campaign, client1)
    opennedEmailViewer.addClient(campaign, client2)


    // SUT : calculate opening rate
    val rate = new  OpenningRateViewer(configuration).openningRate(campaign)

    rate should be (OpenningRate(campaign, Some(0.2), "SUCCESS"))
  }

}
