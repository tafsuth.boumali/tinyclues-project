package com.tinyclues.routes

import com.tinyclues.view.CampaignReferential
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.mock.MockitoSugar
import spray.routing.HttpService
import org.mockito.BDDMockito._
import spray.http.StatusCodes
import spray.testkit.ScalatestRouteTest

class CampaignReferentialRouteSpec extends FlatSpec
  with ScalatestRouteTest
  with CampaignReferentialRoute
  with MockitoSugar
  with Matchers
  with HttpService {


  override implicit def actorRefFactory = system

  override lazy val campaignReferential: CampaignReferential = mock[CampaignReferential]

  it must "put a targetNumber for a campaign" in {
    given(campaignReferential.addCampaignTarget("campaignFnac", 5000)).willReturn(true)
    Put("/campaign/campaignFnac/target/5000") ~> referentialRoute ~> check {
      assert(status === StatusCodes.Created)
    }
  }

  it must "return bad request when action is not performed" in {
    given(campaignReferential.addCampaignTarget("campaignFnac", 5000)).willReturn(false)
    Put("/campaign/campaignFnac/target/5000") ~> referentialRoute ~> check {
      assert(status === StatusCodes.BadRequest)
    }
  }

}
