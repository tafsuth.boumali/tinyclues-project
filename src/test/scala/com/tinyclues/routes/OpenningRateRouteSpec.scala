package com.tinyclues.routes

import com.tinyclues.view.{OpennedEmailViewer, OpenningRate, OpenningRateViewer}
import org.mockito.BDDMockito._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import spray.http.StatusCodes
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.concurrent.Future

/**
  * Created by tafsuthboumali on 30/01/2017.
  */
class OpenningRateRouteSpec extends FlatSpec
  with ScalatestRouteTest
  with OpenningRateRoute
  with MockitoSugar
  with Matchers
  with HttpService {


  override implicit def actorRefFactory = system

  override lazy val openningRateViewer: OpenningRateViewer = mock[OpenningRateViewer]

  import com.tinyclues.routes.OpenningRateProtocol._

  it must "return OpenningRate" in {
    given(openningRateViewer.openningRate("campaignFnac")).willReturn(OpenningRate("campaignFnac", Some(10.0), "SUCCESS"))
    Get("/campaign/campaignFnac/openningRate") ~> openningRateRoute ~> check {
      assert(status === StatusCodes.OK)
      val response = responseAs[OpenningRate]
      assert(response.openningRate === Some(10.0))
    }
  }

  it must "not add new client to campaign but still return Accepted Status Code" in {
    given(openningRateViewer.openningRate("campaignFnac")).willReturn(OpenningRate("campaignFnac",None, "the reason"))
    Get("/campaign/campaignFnac/openningRate") ~> openningRateRoute ~> check {
      assert(status === StatusCodes.OK)
      val response = responseAs[OpenningRate]
      assert(response.openningRate === None)
    }
  }

}
