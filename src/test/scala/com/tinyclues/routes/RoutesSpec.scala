package com.tinyclues.routes

import java.io.IOException
import java.net.{SocketException, SocketTimeoutException}

import com.redis.RedisConnectionException
import com.tinyclues.view.{CampaignReferential, OpennedEmailViewer, OpenningRate, OpenningRateViewer}
import org.mockito.BDDMockito._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import spray.http.StatusCodes
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest


class RoutesSpec extends FlatSpec
  with ScalatestRouteTest
  with Routes
  with MockitoSugar
  with Matchers
  with HttpService {

  val opennedEmailViewer: OpennedEmailViewer = mock[OpennedEmailViewer]
  val campaignReferential: CampaignReferential = mock[CampaignReferential]
  val openningRateViewer: OpenningRateViewer = mock [OpenningRateViewer]


  override implicit def actorRefFactory = system


  it must "get the openingRate when using whole url " in {
    import com.tinyclues.routes.OpenningRateProtocol._
    given(openningRateViewer.openningRate("campaignFnac")).willReturn(OpenningRate("campaignFnac", Some(0.2), "SUCCESS"))
    Get("/tinyclues/api/v1.0/campaigns/campaign/campaignFnac/openningRate") ~> routes ~> check {
      assert(status === StatusCodes.OK)
      val response = responseAs[OpenningRate]
      assert(response.openningRate === Some(0.2))
    }
  }

  it must "handle Redis exception when using whole uri" in {
    given(campaignReferential.addCampaignTarget("campaignFnac", 5000)).willThrow(new RedisConnectionException(""))
    Put("/tinyclues/api/v1.0/campaigns/campaign/campaignFnac/target/5000") ~> routes ~> check {
      assert(status === StatusCodes.InternalServerError)
    }
  }

  it must "return swagger " in {
    Get("/tinyclues/api/v1.0/campaigns/swagger.yaml") ~> routes ~> check {
      assert(status === StatusCodes.OK)
    }
  }


}
