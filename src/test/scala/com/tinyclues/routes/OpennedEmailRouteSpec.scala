package com.tinyclues.routes

import com.tinyclues.view.OpennedEmailViewer
import org.mockito.BDDMockito._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import spray.http.StatusCodes
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.concurrent.Future

/**
  * Created by tafsuthboumali on 30/01/2017.
  */
class OpennedEmailRouteSpec extends FlatSpec
  with ScalatestRouteTest
  with OpennedEmailRoute
  with MockitoSugar
  with Matchers
  with HttpService {


  override implicit def actorRefFactory = system

  override lazy val opennedEmailViewer: OpennedEmailViewer  = mock[OpennedEmailViewer]


  it must "return Accepted StatusCode when add client" in {
    given(opennedEmailViewer.addClient("campaignFnac", "Tafsuth")).willReturn(Future(Some(1l)))
    Get("/campaign/campaignFnac/client/Tafsuth") ~> opennedEmailRoute ~> check {
      assert(status === StatusCodes.Accepted)
    }
  }

  it must "not add new client to campaign but still return Accepted Status Code" in {
    given(opennedEmailViewer.addClient("campaignFnac", "Tafsuth")).willReturn(Future(None))
    Get("/campaign/campaignFnac/client/Tafsuth") ~> opennedEmailRoute ~> check {
      assert(status === StatusCodes.Accepted)
    }
  }

}
