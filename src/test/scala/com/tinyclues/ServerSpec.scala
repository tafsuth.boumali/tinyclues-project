package com.tinyclues

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import spray.http._
import spray.client.pipelining._

import scala.concurrent.Await
import scala.concurrent.duration._

class ServerSpec extends TestKit(ActorSystem("ServerSpec")) with Server with WordSpecLike with BeforeAndAfterAll {

  import scala.collection.JavaConversions._

  import scala.concurrent.ExecutionContext.Implicits.global
  val configurationForServer = ConfigFactory.parseMap(Map(
    "server.listeningInterface" -> "127.0.0.1",
    "server.listeningPort" -> "3949",
    "server.bindingTimeout" -> "2 seconds",
    "redis.host" -> "0.0.0.0",
    "redis.port" -> "6379"))

  start(configurationForServer)

  val pipeline = sendReceive

  "server" must {
    "give a response" in {
      val responseFuture = pipeline(Get("http://localhost:3949/nimporte"))
      val response = Await.result(responseFuture, 5.seconds)
      assert(StatusCodes.NotFound === response.status)
    }
  }

  override def afterAll {
    shutdown()
  }


}
