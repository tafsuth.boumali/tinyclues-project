package com.tinyclues.view

import com.redis.RedisClient
import com.redis.serialization.Parse.Implicits.parseDouble
import org.mockito.Mockito.{times, verify, when}
class OpenningRateViewerSpec extends Base {


  val viewer = new OpenningRateViewer(configuration) {
    override lazy val client: RedisClient = rclient
  }

  it should "get infos from redis in order to calculate the openning rate" in {
    when(viewer.client.get[Double]("ref-campaign-fnacMacBook")).thenReturn(Some(5000.0))
    when(viewer.client.pfcount("openned-campaign-fnacMacBook")).thenReturn(Some(10l))

    viewer.getOpenningRate("fnacMacBook")

    verify(viewer.client, times(1)).get[Double]("ref-campaign-fnacMacBook")
    verify(viewer.client, times(1)).pfcount("openned-campaign-fnacMacBook")
  }

  it should "return the opening rate for a given campaignId" in {
    when(viewer.client.get[Double]("ref-campaign-fnacMacBook")).thenReturn(Some(5000.0))
    when(viewer.client.pfcount("openned-campaign-fnacMacBook")).thenReturn(Some(10l))

    val res = viewer.getOpenningRate("fnacMacBook")

    assert (res._1 === Some(0.002))

  }

  it should "return None when target number has never been defined" in {
    when(viewer.client.get[Double]("ref-campaign-fnacMacBook")).thenReturn(Some(0.0))
    when(viewer.client.pfcount("openned-campaign-fnacMacBook")).thenReturn(Some(10l))

    val res = viewer.getOpenningRate("fnacMacBook")

    assert (res._1 === None)

  }

  it should "return None when there is error parsing target number" in {
    when(viewer.client.get[Double]("ref-campaign-fnacMacBook")).thenReturn(None)
    when(viewer.client.pfcount("openned-campaign-fnacMacBook")).thenReturn(Some(10l))

    val res = viewer.getOpenningRate("fnacMacBook")

    assert (res._1 === None)

  }

  it should "return 0.0 when no email has been openned" in {
    when(viewer.client.get[Double]("ref-campaign-fnacMacBook")).thenReturn(Some(5000.0))
    when(viewer.client.pfcount("openned-campaign-fnacMacBook")).thenReturn(None)

    val res = viewer.getOpenningRate("fnacMacBook")

    assert (res._1 === Some(0.0))

  }

  it should "return 0.0 when pfcount returns None (???)" in {
    when(viewer.client.get[Double]("ref-campaign-fnacMacBook")).thenReturn(Some(5000.0))
    when(viewer.client.pfcount("openned-campaign-fnacMacBook")).thenReturn(Some(0l))

    val res = viewer.getOpenningRate("fnacMacBook")

    assert (res._1 === Some(0.0))

  }












}
