package com.tinyclues.view

import com.redis.RedisClient
import com.tinyclues.ApiConfiguration
import com.typesafe.config.ConfigFactory
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import org.mockito.Mockito._

abstract class Base extends FlatSpec with Matchers with MockitoSugar with BeforeAndAfterAll {

  val rclient: RedisClient = mock[RedisClient]

  val configuration = new ApiConfiguration(ConfigFactory.load())

}
