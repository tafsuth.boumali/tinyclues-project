package com.tinyclues.view

import com.redis.RedisClient
import org.mockito.Mockito._

class OpennedEmailViewerSpec extends Base {

  val viewer = new OpennedEmailViewer(configuration) {
    override lazy val redisClient: RedisClient = rclient
  }

  ignore should "add client to hyperloglog" in {

    when(viewer.redisClient.pfadd("openned-campaign-fnacMacBook", "tafsuth")).thenReturn(Some(1l))

    viewer.addClient("fnacMacBook", "tafsuth")

    verify(viewer.redisClient, times(1)).pfadd("openned-campaign-fnacMacBook", "tafsuth")
  }
}
