package com.tinyclues.view

import org.mockito.Mockito.{times, verify, when}


class TargetClientReferentialSpec extends Base  {

  val viewer = new CampaignReferential(configuration) {
    override lazy val client = rclient
  }

  it should "add campaign with its target number of clients" in {
    when(viewer.client.set("ref-campaign-fnacMacBook", 5000)).thenReturn(true)

    viewer.addCampaignTarget("fnacMacBook", 5000)

    verify(viewer.client, times(1)).set("ref-campaign-fnacMacBook", 5000)
  }
}
