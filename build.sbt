name := "opening-rate"

version := "1.0"

scalaVersion := "2.11.8"

resolvers ++= Seq( "Maven Central" at "http://repo1.maven.org/maven2/",
  "Spray.io Releases" at "http://repo.spray.io/",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases",
  Resolver.bintrayRepo("iheartradio","maven"))

libraryDependencies ++= {
  val akkaVersion       = "2.3.9"
  val sprayVersion      = "1.3.3"
  Seq(
    "com.typesafe.akka" %% "akka-actor"      % akkaVersion,
    "io.spray"          %% "spray-can"       % sprayVersion,
    "io.spray"          %% "spray-routing"   % sprayVersion,
    "io.spray"          %% "spray-client"   % sprayVersion % "test,it",
    "io.spray"          %% "spray-json"      % sprayVersion,
    "ch.qos.logback"    %  "logback-classic" % "1.1.2",
    "com.typesafe.akka" %% "akka-testkit"    % akkaVersion  % "test",
    "io.spray"          %% "spray-testkit"   % sprayVersion % "test",
    "org.specs2"        %% "specs2"          % "2.3.13"     % "test",
    "org.scalatest" %% "scalatest" % "2.2.5" % "test,it",
    "com.iheart" %% "ficus" % "1.1.3" exclude ("com.chuusai", "shapeless") exclude ("com.typesafe", "config") exclude ("org.scala-lang", "scala-library") exclude ("org.scala-lang", "scala-reflect") exclude ("org.scalacheck", "scalacheck") exclude ("org.specs2", "specs2"),
    "net.debasishg" %% "redisclient" % "3.3",
    "org.mockito" % "mockito-all" % "1.8.4"  % "test"
  )
}

concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)

Defaults.itSettings
lazy val root = project.in(file(".")).configs(IntegrationTest)

mainClass in Global := Some("com.tinyclues.ServerBoot")

